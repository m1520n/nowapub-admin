import {
  LOGIN_ATTEMPT,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT,
  FETCH_PRODUCTS_ATTEMPT,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_FAILURE,
  CREATE_PRODUCT_ATTEMPT,
  CREATE_PRODUCT_SUCCESS,
  CREATE_PRODUCT_FAILURE,
  UPDATE_PRODUCT_ATTEMPT,
  UPDATE_PRODUCT_SUCCESS,
  UPDATE_PRODUCT_FAILURE,
  UPDATE_PRODUCT_ORDER_ATTEMPT,
  UPDATE_PRODUCT_ORDER_SUCCESS,
  UPDATE_PRODUCT_ORDER_FAILURE,
  DELETE_PRODUCT_ATTEMPT,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_FAILURE,
  FETCH_MENU_CATEGORIES_ATTEMPT,
  FETCH_MENU_CATEGORIES_SUCCESS,
  FETCH_MENU_CATEGORIES_FAILURE,
} from '../actions';

const authenticated = !!window.localStorage.getItem('user');

const initialState = {
  menuCategories: [],
  products: [],
  authenticated,
  loggingIn: false,
  fetchingProducts: false,
  fetchingMenuCategories: false,
  creatingProduct: false,
  updatingProduct: false,
};

const updateObjectInArray = (array, updatedElement, id) =>
  array.map(item => {
    if (item._id !== id) {
      return item;
    }

    return {
      ...item,
      ...updatedElement,
    };
  });

const addObjectToArray = (array, element) => [...array.map(obj => ({ ...obj })), element];

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_ATTEMPT:
      return {
        ...state,
        loggingIn: true,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        loggingIn: false,
        authenticated: true,
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        loggingIn: false,
        authenticated: false,
      };
    case LOGOUT:
      return {
        ...state,
        authenticated: false,
      };
    case FETCH_PRODUCTS_ATTEMPT:
      return {
        ...state,
        fetchingProducts: true,
      };
    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        fetchingProducts: false,
        products: action.products,
      };
    case FETCH_PRODUCTS_FAILURE:
      return {
        ...state,
        fetchingProducts: false,
      };
    case CREATE_PRODUCT_ATTEMPT:
      return {
        ...state,
        creatingProduct: true,
      };
    case CREATE_PRODUCT_SUCCESS:
      const updatedProducts = addObjectToArray(state.products, action.product);
      return {
        ...state,
        creatingProduct: false,
        products: updatedProducts,
      };
    case CREATE_PRODUCT_FAILURE:
      return {
        ...state,
        creatingProduct: false,
      };
    case UPDATE_PRODUCT_ATTEMPT:
      return {
        ...state,
        updatingProduct: true,
      };
    case UPDATE_PRODUCT_SUCCESS:
      return {
        ...state,
        updatingProduct: false,
        products: updateObjectInArray(state.products, action.product, action.product._id),
      };
    case UPDATE_PRODUCT_FAILURE:
      return {
        ...state,
        updatingProduct: false,
      };
    case UPDATE_PRODUCT_ORDER_ATTEMPT:
      return {
        ...state,
        updatingProductOrder: true,
      };
    case UPDATE_PRODUCT_ORDER_SUCCESS:
      const { updatedSource, updatedDestination } = action;
      const productsWithUpdatedSource = updateObjectInArray(state.products, updatedSource, updatedSource._id);
      const productsWithUpdatedDestination = updateObjectInArray(
        productsWithUpdatedSource,
        updatedDestination,
        updatedDestination._id,
      );
      return {
        ...state,
        updatingProductOrder: false,
        products: productsWithUpdatedDestination,
      };
    case UPDATE_PRODUCT_ORDER_FAILURE:
      return {
        ...state,
        updatingProductOrder: false,
      };
    case DELETE_PRODUCT_ATTEMPT:
      return {
        ...state,
        deletingProduct: true,
      };
    case DELETE_PRODUCT_SUCCESS:
      return {
        ...state,
        products: state.products.filter(product => product._id !== action.id),
        deletingProduct: false,
      };
    case DELETE_PRODUCT_FAILURE:
      return {
        ...state,
        deletingProduct: false,
      };
    case FETCH_MENU_CATEGORIES_ATTEMPT:
      return {
        ...state,
        fetchingMenuCategories: true,
      };
    case FETCH_MENU_CATEGORIES_SUCCESS:
      return {
        ...state,
        fetchingMenuCategories: false,
        menuCategories: action.categories,
      };
    case FETCH_MENU_CATEGORIES_FAILURE:
      return {
        ...state,
        fetchingMenuCategories: false,
      };
    default:
      return state;
  }
};

export default rootReducer;
