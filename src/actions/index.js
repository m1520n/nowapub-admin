import { message } from 'antd';

import { getToken } from '../services/auth';
import { history } from '../helpers/router';
import { fetchProducts, createProduct, updateProduct, deleteProduct, fetchMenuCategories } from '../services/product';

// Action types
export const LOGIN_ATTEMPT = 'LOGIN_ATTEMPT';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export const LOGOUT = 'LOGOUT';

export const FETCH_PRODUCTS_ATTEMPT = 'FETCH_PRODUCTS_ATTEMPT';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_FAILURE = 'FETCH_PRODUCTS_FAILURE';

export const CREATE_PRODUCT_ATTEMPT = 'CREATE_PRODUCT_ATTEMPT';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';
export const CREATE_PRODUCT_FAILURE = 'CREATE_PRODUCT_FAILURE';

export const UPDATE_PRODUCT_ATTEMPT = 'UPDATE_PRODUCT_ATTEMPT';
export const UPDATE_PRODUCT_SUCCESS = 'UPDATE_PRODUCT_SUCCESS';
export const UPDATE_PRODUCT_FAILURE = 'UPDATE_PRODUCT_FAILURE';

export const UPDATE_PRODUCT_ORDER_ATTEMPT = 'UPDATE_PRODUCT_ORDER_ATTEMPT';
export const UPDATE_PRODUCT_ORDER_SUCCESS = 'UPDATE_PRODUCT_ORDER_SUCCESS';
export const UPDATE_PRODUCT_ORDER_FAILURE = 'UPDATE_PRODUCT_ORDER_FAILURE';

export const DELETE_PRODUCT_ATTEMPT = 'DELETE_PRODUCT_ATTEMPT';
export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';
export const DELETE_PRODUCT_FAILURE = 'DELETE_PRODUCT_FAILURE';

export const FETCH_MENU_CATEGORIES_ATTEMPT = 'FETCH_MENU_CATEGORIES_ATTEMPT';
export const FETCH_MENU_CATEGORIES_SUCCESS = 'FETCH_MENU_CATEGORIES_SUCCESS';
export const FETCH_MENU_CATEGORIES_FAILURE = 'FETCH_MENU_CATEGORIES_FAILURE';

// Action creators
export const login = payload => async dispatch => {
  dispatch({ type: LOGIN_ATTEMPT });
  try {
    const token = await getToken(payload);
    localStorage.setItem('user', token);
    history.push('/');
    dispatch({ type: LOGIN_SUCCESS });
  } catch (error) {
    dispatch({ type: LOGIN_FAILURE });
    message.error('Nieprawidłowy login lub hasło');
  }
};

// Action creators
export const logout = () => async dispatch => {
  dispatch({ type: LOGOUT });
  localStorage.removeItem('user');
  history.push('/login');
};

export const createFetchProducts = () => async dispatch => {
  dispatch({ type: FETCH_PRODUCTS_ATTEMPT });
  try {
    const products = await fetchProducts();
    dispatch({ type: FETCH_PRODUCTS_SUCCESS, products });
  } catch (error) {
    dispatch({ type: FETCH_PRODUCTS_FAILURE });
  }
};

export const createCreateProduct = payload => async dispatch => {
  dispatch({ type: CREATE_PRODUCT_ATTEMPT });
  try {
    const product = await createProduct(payload);
    dispatch({ type: CREATE_PRODUCT_SUCCESS, product });
  } catch (error) {
    dispatch({ type: CREATE_PRODUCT_FAILURE });
  }
};

export const createUpdateProduct = payload => async dispatch => {
  dispatch({ type: UPDATE_PRODUCT_ATTEMPT });
  try {
    const product = await updateProduct(payload);
    dispatch({ type: UPDATE_PRODUCT_SUCCESS, product });
  } catch (error) {
    dispatch({ type: UPDATE_PRODUCT_FAILURE });
  }
};

export const createUpdateProductOrder = payload => async dispatch => {
  const { source, destination } = payload;

  dispatch({ type: UPDATE_PRODUCT_ORDER_ATTEMPT });
  try {
    const updatedSource = await updateProduct({
      _id: destination._id,
      order: source.order,
    });
    const updatedDestination = await updateProduct({
      _id: source._id,
      order: destination.order,
    });
    dispatch({ type: UPDATE_PRODUCT_ORDER_SUCCESS, updatedSource, updatedDestination });
  } catch (error) {
    dispatch({ type: UPDATE_PRODUCT_ORDER_FAILURE });
  }
};

export const createDeleteProduct = id => async dispatch => {
  dispatch({ type: DELETE_PRODUCT_ATTEMPT, id });
  try {
    await deleteProduct(id);
    dispatch({ type: DELETE_PRODUCT_SUCCESS, id });
  } catch (error) {
    dispatch({ type: DELETE_PRODUCT_FAILURE });
  }
};

export const createFetchMenuCategories = () => async dispatch => {
  dispatch({ type: FETCH_MENU_CATEGORIES_ATTEMPT });
  try {
    const categories = await fetchMenuCategories();
    dispatch({ type: FETCH_MENU_CATEGORIES_SUCCESS, categories });
  } catch (error) {
    dispatch({ type: FETCH_MENU_CATEGORIES_FAILURE });
  }
};
