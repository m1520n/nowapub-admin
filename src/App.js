import React, { Component, Fragment } from 'react';
import { Router, Route, withRouter, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import { logout, createFetchMenuCategories, createFetchProducts } from './actions';
import { history } from './helpers/router';

import { Layout } from 'antd';

import Home from './components/Home';
import Products from './components/Products';
import Login from './components/Login';
import PrivateRoute from './components/PrivateRoute';
import NotFound from './components/404';

import './App.css';

import HeaderNavigation from './components/HeaderNavigation';
import Navigation from './components/Navigation';
import styled from 'styled-components';

const { Content } = Layout;

const NavigationWithRouter = withRouter(Navigation);

const StyledContent = styled(Content)`
  padding: 6rem 1rem;
  text-align: center;

  @media (min-width: 993px) {
    text-align: left;
    padding: 6rem 3rem;
    margin-left: ${({ authenticated }) => (authenticated === 'true' ? '12.5rem' : 0)};
  }
`;

class App extends Component {
  constructor() {
    super();

    this.state = {
      collapsed: false,
    };
  }

  componentDidMount() {
    this.props.fetchMenuCategories();
    this.props.fetchProducts();
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  onLogout = e => {
    e.preventDefault();
    this.props.logout();
  };

  render() {
    const { menuCategories, authenticated } = this.props;
    const { collapsed } = this.state;
    const { Sider } = Layout;
    return (
      <Router history={history}>
        <Fragment>
          <HeaderNavigation onLogout={this.onLogout} authenticated={authenticated} />
          {authenticated ? (
            <Sider style={{ position: 'fixed', zIndex: 3, height: '100vh' }} breakpoint="lg" collapsedWidth="0">
              <NavigationWithRouter
                toggleCollapsed={this.toggleCollapsed}
                menuCategories={menuCategories}
                collapsed={collapsed}
              />
            </Sider>
          ) : null}
          <StyledContent authenticated={authenticated.toString()}>
            <Switch>
              <PrivateRoute exact path="/" component={Home} />
              <PrivateRoute path="/produkty/:id" component={Products} menuCategories={menuCategories} />
              <Route path="/login" component={Login} />
              <PrivateRoute component={NotFound} />
            </Switch>
          </StyledContent>
        </Fragment>
      </Router>
    );
  }
}

const mapDispatchToProps = {
  logout,
  fetchMenuCategories: createFetchMenuCategories,
  fetchProducts: createFetchProducts,
};
const mapStateToProps = ({ menuCategories, authenticated }) => ({
  menuCategories,
  authenticated,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
