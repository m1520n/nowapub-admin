import fetch from 'cross-fetch';

import config from '../config';
const apiUrl = `${config.api[process.env.API_ENV]}/api`;

export const getToken = async body => {
  try {
    const response = await fetch(`${apiUrl}/auth/login`, {
      method: 'post',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    const { token } = await response.json();

    if (!token) throw new Error('Nieprawidłowy login lub hasło');

    return token;
  } catch (error) {
    throw new Error(error);
  }
};
