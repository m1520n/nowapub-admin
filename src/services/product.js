import fetch from 'cross-fetch';

import config from '../config';
import { authHeader } from '../helpers/auth-header';
const apiUrl = `${config.api[process.env.API_ENV]}/api`;

export const fetchProducts = async body => {
  try {
    const response = await fetch(`${apiUrl}/products`);

    const { products } = await response.json();

    return products;
  } catch (error) {
    throw new Error(error);
  }
};

export const createProduct = async body => {
  try {
    const response = await fetch(`${apiUrl}/products/`, {
      method: 'post',
      body: JSON.stringify({
        ...body,
        visible: true,
      }),
      headers: {
        ...authHeader(),
        'Content-Type': 'application/json',
      },
    });
    const { product } = await response.json();

    return product;
  } catch (error) {
    throw new Error(error);
  }
};

export const updateProduct = async body => {
  try {
    const response = await fetch(`${apiUrl}/products/${body._id}`, {
      method: 'put',
      body: JSON.stringify(body),
      headers: {
        ...authHeader(),
        'Content-Type': 'application/json',
      },
    });
    const { product } = await response.json();

    return product;
  } catch (error) {
    throw new Error(error);
  }
};

export const deleteProduct = async id => {
  try {
    await fetch(`${apiUrl}/products/${id}`, {
      method: 'delete',
      headers: { ...authHeader() },
    });
  } catch (error) {
    throw new Error(error);
  }
};

export const fetchMenuCategories = async body => {
  try {
    const response = await fetch(`${apiUrl}/menu-categories`);

    const { menuCategories } = await response.json();

    return menuCategories;
  } catch (error) {
    throw new Error(error);
  }
};
