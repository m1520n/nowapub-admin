import React from 'react';
import { Layout, Row } from 'antd';

const Navigation = ({ onLogout, authenticated }) => {
  return (
    <Layout.Header style={{ position: 'fixed', zIndex: 3, width: '100%' }}>
      <Row style={{ padding: '0', textAlign: 'right' }}>
        {authenticated ? (
          <a href="/login" onClick={onLogout}>
            Wyloguj się
          </a>
        ) : null}
      </Row>
    </Layout.Header>
  );
};

export default Navigation;
