import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import { createCreateProduct, createDeleteProduct, createUpdateProduct, createUpdateProductOrder } from '../actions';

import ProductsTable from './ProductsTable';
import WrapperProductForm from './ProductForm';

import { message, Button, Modal, Spin, Layout } from 'antd';

const { Content } = Layout;

class Products extends Component {
  constructor(props) {
    super(props);

    this.state = {
      filteredProducts: [],
      createModalVisible: false,
      editModalVisible: false,
      deleteModalVisible: false,
      editedProduct: {},
    };
  }

  handleCancel = () => {
    this.setState({
      createModalVisible: false,
      editModalVisible: false,
      deleteModalVisible: false,
    });
  };
  prepareProductPayload = product => ({
    name: product.name,
    description: product.description,
    category: this.props.match.params.id,
    customFields: Object.keys(product)
      .filter(key => !['_id', 'category', 'name', 'description'].includes(key))
      .map(k => ({ name: k, value: product[k] })),
  });
  handleCreateProduct = product => {
    this.props.createProduct(this.prepareProductPayload(product));
    this.setState({
      createModalVisible: false,
    });
  };

  handleUpdateProduct = product => {
    console.log(product);
    this.props.updateProduct({
      _id: product._id,
      ...this.prepareProductPayload(product),
    });
    this.setState({
      editModalVisible: false,
    });
  };

  handleDeleteProduct = async id => {
    this.props.deleteProduct(id);
  };

  handleConfirmDelete = async () => {
    await this.deleteProduct();
    message.success('Pomyślnie usunięto produkt');
  };

  showCreateProductModal = id => {
    this.setState({
      createModalVisible: true,
    });
  };

  showEditProductModal = editedProduct => {
    this.setState({
      editModalVisible: true,
      editedProduct,
    });
  };

  showDeleteModal = () => {
    this.setState({
      deleteModalVisible: true,
    });
  };

  handleOrderUp = id => {
    const { updateProductOrder, filteredProducts } = this.props;
    const movedProduct = filteredProducts.find(p => p._id === id);
    const index = filteredProducts.indexOf(movedProduct);
    const prevProduct = filteredProducts[index - 1];

    updateProductOrder({ source: movedProduct, destination: prevProduct });
  };

  handleOrderDown = id => {
    const { updateProductOrder, filteredProducts } = this.props;
    const movedProduct = filteredProducts.find(p => p._id === id);
    const index = filteredProducts.indexOf(movedProduct);
    const nextProduct = filteredProducts[index + 1];

    updateProductOrder({ source: movedProduct, destination: nextProduct });
  };

  render() {
    const { deleteModalVisible, editModalVisible, createModalVisible, editedProduct } = this.state;
    const {
      filteredProducts,
      menuCategories,
      fetchingProducts,
      fetchingMenuCategories,
      creatingProduct,
      updatingProduct,
    } = this.props;

    const selectedCategory = menuCategories.find(c => c._id === this.props.match.params.id);
    const categoryName = selectedCategory && selectedCategory.name;
    return (
      <Content
        style={{
          position: 'relative',
          minHeight: '90vh',
          width: '100%',
        }}
      >
        {fetchingProducts || fetchingMenuCategories ? (
          <Spin
            size="large"
            style={{
              position: 'absolute',
              top: '50%',
              left: '50%',
              transform: '-50%, -50%',
            }}
          />
        ) : (
          <Fragment>
            <h1 style={{ textTransform: 'uppercase' }}>{categoryName}</h1>
            <Button
              onClick={this.showCreateProductModal}
              style={{ margin: '2rem 0' }}
              type="primary"
              size="large"
              block="true"
            >
              Dodaj nowy produkt
            </Button>
            <ProductsTable
              onDeleteProduct={this.handleDeleteProduct}
              onEditProduct={this.showEditProductModal}
              handleCancel={this.handleCancel}
              handleUpdateProduct={this.handleUpdateProduct}
              deleteModalVisible={deleteModalVisible}
              products={filteredProducts}
              handleOrderUp={this.handleOrderUp}
              handleOrderDown={this.handleOrderDown}
            />
            <Modal title="Dodawanie pozycji" visible={createModalVisible} footer={null} onCancel={this.handleCancel}>
              <WrapperProductForm onSubmit={this.handleCreateProduct} menuCategory={selectedCategory} />
            </Modal>
            <Modal title="Edycja pozycji" visible={editModalVisible} footer={null} onCancel={this.handleCancel}>
              <WrapperProductForm
                product={editedProduct}
                onSubmit={this.handleUpdateProduct}
                menuCategory={selectedCategory}
                creatingProduct={creatingProduct}
                updatingProduct={updatingProduct}
              />
            </Modal>
          </Fragment>
        )}
      </Content>
    );
  }
}

const mapDispatchToProps = {
  deleteProduct: createDeleteProduct,
  updateProduct: createUpdateProduct,
  updateProductOrder: createUpdateProductOrder,
  createProduct: createCreateProduct,
};

const mapStateToProps = (
  { products, menuCategories, fetchingProducts, fetchingMenuCategories, creatingProduct, updatingProduct },
  ownProps,
) => ({
  products,
  filteredProducts: products
    .filter(
      product => product.category === ownProps.match.params.id || product.category._id === ownProps.match.params.id,
    )
    .sort((a, b) => a.order - b.order),
  menuCategories,
  fetchingProducts,
  fetchingMenuCategories,
  creatingProduct,
  updatingProduct,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Products);
