import React, { Fragment } from 'react';
import { Table, Icon, Popconfirm, Button } from 'antd';

const ProductsTable = ({ products, onDeleteProduct, onEditProduct, handleOrderUp, handleOrderDown }) => {
  const columns = [
    {
      title: 'Lp',
      dataIndex: 'order',
      key: 'order',
      width: '5%',
    },
    {
      title: 'Nazwa',
      dataIndex: 'name',
      key: 'name',
      width: '15%',
    },
    {
      title: 'Opis',
      dataIndex: 'description',
      key: 'description',
      width: '80%',
    },
    {
      title: 'Edycja',
      dataIndex: 'edit',
      key: 'edit',
    },
    {
      title: 'Usuń',
      dataIndex: 'delete',
      key: 'delete',
    },
  ];
  const dataSource = products.map((product, index) => ({
    key: index,
    order: (
      <Fragment>
        <Button onClick={() => handleOrderUp(product._id)} size="small" disabled={index === 0 ? 'disabled' : ''}>
          <Icon type="caret-up" />
        </Button>
        <Button
          onClick={() => handleOrderDown(product._id)}
          size="small"
          disabled={index === products.length - 1 ? 'disabled' : ''}
        >
          <Icon type="caret-down" />
        </Button>
      </Fragment>
    ),
    name: product.name,
    description: product.description,
    edit: (
      <Button onClick={() => onEditProduct(product)}>
        <Icon type="edit" />
      </Button>
    ),
    delete: (
      <Popconfirm
        placement="leftTop"
        title="Czy napewno chcesz usunąć pozycje?"
        onConfirm={() => onDeleteProduct(product._id)}
        okText="Tak"
        cancelText="Nie"
      >
        <Button>
          <Icon type="delete" />
        </Button>
      </Popconfirm>
    ),
  }));

  return <Table columns={columns} dataSource={dataSource} />;
};

export default ProductsTable;
