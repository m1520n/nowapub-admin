import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Menu, Icon, Layout } from 'antd';
const { Header } = Layout;

const Navigation = props => {
  const { menuCategories, history } = props;
  const currentLocation = history.location.pathname;

  return (
    <Fragment>
      <Header>
        <Icon type="pie-chart" style={{ fontSize: 16, color: '#fff' }} />
        <span style={{ padding: '1rem', fontSize: 16, color: '#fff' }}>Menu</span>
      </Header>
      <Menu mode="inline" defaultSelectedKeys={[currentLocation]}>
        <Menu.Item key={'/'}>
          <Link to="/">Home</Link>
        </Menu.Item>
        {menuCategories.map((category, key) => (
          <Menu.Item key={`/produkty/${category._id}`}>
            <Link to={`/produkty/${category._id}`}>{category.name.toUpperCase()}</Link>
          </Menu.Item>
        ))}
      </Menu>
    </Fragment>
  );
};

export default Navigation;
