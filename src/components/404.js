import React from 'react';
import { Layout } from 'antd';

const { Content } = Layout;

const Home = () => (
  <Content
    style={{
      width: '100%',
      height: '75vh',
      textAlign: 'center',
    }}
  >
    <h1>Przykro nam, nie znaleziono takiej strony!</h1>
  </Content>
);

export default Home;
