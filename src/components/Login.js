import React from 'react';
import { connect } from 'react-redux';
import { Form, Icon, Input, Button, Checkbox, Col, Row } from 'antd';

import { history } from '../helpers/router';
import { login } from '../actions';
import styled from 'styled-components';

const StyledForm = styled(Form)`
  padding: 2rem;
  background-color: white;
`;

const StyledH1 = styled.h1`
  padding: 1rem;
  text-align: center;
`;
const FormItem = Form.Item;

class Login extends React.Component {
  constructor(props) {
    super(props);
    if (props.authenticated) history.push('/');
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        await this.props.login(values);
      }
    });
  };

  render() {
    const { loggingIn, form } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Row style={{ height: '100vh' }} type="flex" align="top">
        <Col span={10} offset={8}>
          <StyledH1>Pub Nowa Admin</StyledH1>
          <StyledForm layout="vertical" onSubmit={this.handleSubmit} className="login-form">
            <FormItem>
              {getFieldDecorator('username', {
                rules: [{ required: true, message: 'Proszę wprowadź poprawną nazwę' }],
              })(<Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Proszę wprowadź poprawne hasło' }],
              })(
                <Input
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type="password"
                  placeholder="Password"
                />,
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true,
              })(<Checkbox>zapamiętaj</Checkbox>)}
              <Button type="primary" htmlType="submit" className="login-form-button" loading={loggingIn}>
                Zaloguj
              </Button>
            </FormItem>
          </StyledForm>
        </Col>
      </Row>
    );
  }
}

const mapDispatchToProps = { login };
const mapStateToProps = ({ authenticated, loggingIn }) => ({
  authenticated,
  loggingIn,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form.create()(Login));
