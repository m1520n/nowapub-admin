import React from 'react';
import { Form, Input, InputNumber, Button } from 'antd';

const { TextArea } = Input;

class ProductForm extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit({ ...values, _id: this.props.product && this.props.product._id });
      }
    });
  };

  render() {
    const { form, menuCategory, creatingProduct, updatingProduct } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 18 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 18 },
        sm: { span: 16 },
      },
    };

    const loading = creatingProduct || updatingProduct;

    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Item label="Nazwa" {...formItemLayout}>
          {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Musisz wprowadzić nazwę',
              },
            ],
          })(<Input onChange={this.onTextChange} />)}
        </Form.Item>
        <Form.Item label="Opis" {...formItemLayout}>
          {getFieldDecorator('description', {
            rules: [
              {
                required: true,
                message: 'Musisz wprowadzić opis',
              },
            ],
          })(<TextArea onChange={this.onTextChange} rows="4" />)}
        </Form.Item>
        {menuCategory.additionalFields.map(({ name, type }, index) => (
          <Form.Item key={index} label={name} {...formItemLayout}>
            {getFieldDecorator(`${name}`, {
              rules: [
                {
                  required: true,
                  message: 'Wprowadź poprawne dane',
                },
              ],
            })(<InputNumber onChange={this.onTextChange} step="0.01" />)}
            &nbsp;zł
          </Form.Item>
        ))}
        <Button type="primary" htmlType="submit" className="login-form-button" loading={loading}>
          Zapisz
        </Button>
      </Form>
    );
  }
}

const WrapperProductForm = Form.create({
  mapPropsToFields: props => {
    const customFields = props.product && props.product.customFields;
    const reducedFields = props.product
      ? Object.assign(
          {},
          ...customFields.map(item => ({
            [item.name]: Form.createFormField({ value: item.value || '' }),
          })),
        )
      : {};
    return {
      name: Form.createFormField({ value: (props.product && props.product.name) || '' }),
      description: Form.createFormField({ value: (props.product && props.product.description) || '' }),
      ...reducedFields,
    };
  },
})(ProductForm);

export default WrapperProductForm;
