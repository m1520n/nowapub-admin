export const authHeader = () => {
  let token = localStorage.getItem('user');

  return token !== undefined ? { Authorization: token } : {};
};
